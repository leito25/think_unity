using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controller 2D

    [RequireComponent(typeof(BoxCollider2D))]
    public class Controller2D : MonoBehaviour
    {
        public LayerMask collisionMask;
        
        // tickeness of the collision rays
        private const float skinWidth = .015f;
        
        // how many rays and their spacing
        public int horizontalRayCount = 4;
        public int verticalRayCount = 4;
        
        // rays spacing
        private float horizontalRaySpacing;
        private float verticalRaySpacing;
        
        // refernce to the boxCollider
        private BoxCollider2D mycollider;
        private RaycastOrigins raycastOrigins; // The reference to the collision bounds
        
        //check where collison from, collision info
        public CollisionInfo collisions_inf;
        
        

        private void Start()
        {
            mycollider = GetComponent<BoxCollider2D>();
            CalculateRaySpacing();//just once
        }
        
        //This update the rays
        void UpdateRaycastOrigins()
        {
            Bounds bounds = mycollider.bounds;//reference to the box collider bounds
            bounds.Expand(skinWidth * -2);// and inset border to the rays

            //bounds corners - good code to make some geometrical operations inside an object
            raycastOrigins.bottonLeft = new Vector2(bounds.min.x, bounds.min.y);
            raycastOrigins.bottonRight = new Vector2(bounds.max.x, bounds.min.y);
            raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
            
        }

        
        // The spacing betweeen rays
        void CalculateRaySpacing()
        {
            Bounds bounds = mycollider.bounds; //Bounds
            bounds.Expand(skinWidth * -2); //Inset
            //Slice the space establiching a range - min 2 to max undefines
            horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
            verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);
            //get the between rays space
            horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
            verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);//minus 1 cause is the space between rsys
        }

        public void Move(Vector3 velocity)
        {
            //Calculate the ray just when moves
            UpdateRaycastOrigins();
            collisions_inf.Reset();//reset collison info evevy move frame
            
            //vertical collision
            if (velocity.x != 0)
            {
                HorizontalCollision(ref velocity);
            }
            if (velocity.y != 0)
            {
                VerticalCollision(ref velocity); //passing as reference to no generate copies
            }
            
            //move function
            transform.Translate(velocity);
            
            //Log
            Debug.Log("Up: " + collisions_inf.above );
            Debug.Log("Down: " + collisions_inf.below );
            Debug.Log("Left: " + collisions_inf.left );
            Debug.Log("Right: " + collisions_inf.right );
        }
        
        // horizontal collision
        void HorizontalCollision(ref Vector3 velocity)
        {
            //draw the cuantity of velocity
            float directionX = Mathf.Sign(velocity.x);//check the sign(+ or -) to show the ray direction.
            float rayLenght = Mathf.Abs(velocity.x) + skinWidth;//lenght of the velocity vector + skinwidth border
            
            //Draw the rays
            for (int i = 0; i < horizontalRayCount; i++)
            {
                //doing the collision
                // set the origin from botton left or top left //negative is -1
                Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottonLeft : raycastOrigins.bottonRight; 
                rayOrigin += Vector2.up * (horizontalRaySpacing * i );
                
                //Raycast hit
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLenght, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLenght,Color.red );
                
                //Ray hit collision
                if (hit)
                {
                    velocity.x = (hit.distance - skinWidth) * directionX;
                    rayLenght = hit.distance;
                    
                    //after hitted.. check the coll info
                    collisions_inf.left = directionX == -1;
                    collisions_inf.right = directionX == 1;
                    //setting the direction of the collision on the CollisonInfo struct
                }
            }
        }
        
        //the vertical collision rays calcule
        void VerticalCollision(ref Vector3 velocity)
        {
            //draw the cuantity of velocity
            float directionY = Mathf.Sign(velocity.y);//check the sign(+ or -) to show the ray direction.
            float rayLenght = Mathf.Abs(velocity.y) + skinWidth;//lenght of the velocity vector + skinwidth border
            
            //Draw the rays
            for (int i = 0; i < verticalRayCount; i++)
            {
                //doing the collision
                // set the origin from botton left or top left //negative is -1
                Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottonLeft : raycastOrigins.topLeft; 
                rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);
                
                //Raycast hit
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLenght, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.up * (directionY * rayLenght),Color.red );
                
                //Ray hit collision
                if (hit)
                {
                    velocity.y = (hit.distance - skinWidth) * directionY;
                    rayLenght = hit.distance;
                    
                    //after hitted.. check the coll info
                    collisions_inf.below = directionY == -1;
                    collisions_inf.above = directionY == 1;
                    //setting the direction of the collision on the CollisonInfo struct
                }
            }
        }
        
        
        //STRUCTS
        
        
        //Raycast origin box
        struct RaycastOrigins // A simple struct with the coordinates
        {
            public Vector2 topLeft, topRight;
            public Vector2 bottonLeft, bottonRight;
        }
        
        // Collision info
        public struct CollisionInfo
        {
            public bool above, below;
            public bool left, right;

            // Reset of the values
            public void Reset()
            {
                above = below = false;
                left = right = false;
            }
        }
    }