using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

// Player code
[RequireComponent(typeof(Controller2D))]
    public class Player : MonoBehaviour
    {
        // new jump math
        public float jumpHeight = 4;
        public float timeToJumpApex = .4f;

        private float accellTimeAirborne = .2f;

        private float accellTimeGrounded = .1f;
        // Player properties
        private float moveSpeed = 6;
        private float gravity;
        private float jumpVelocity;
        private Vector3 velocity;

        private float velocityXSmoothing;
        
        private Controller2D controller;

        private void Start()
        {
            controller = GetComponent<Controller2D>();
            
            //kinetick formula
            gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
            jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
            print("Gravity: " + gravity + " Jump Velocity: " + jumpVelocity);
        }

        private void Update()
        {
            // avoid the fast fall when player falls
            // if above and below are the same is cause the player is floating.. or falling
            if (controller.collisions_inf.above || controller.collisions_inf.below)
            {
                velocity.y = 0; //set the velocity to zero to make a natural fall
            }
            
            if (Input.GetKeyDown(KeyCode.Space) && controller.collisions_inf.below)
            {
                velocity.y = jumpVelocity;
            }
            
            //Horizontal and vertical input
            Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            
            // hrizontal movement controlled by input
            velocity.x = input.x * moveSpeed;
            //adding a little bit of smoothness
            float targetVelocityX = input.x * moveSpeed;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions_inf.below) ? accellTimeGrounded : accellTimeAirborne);
            
            
            // add the velocity to the player
            velocity.y += gravity * Time.deltaTime; //not using rigid body just maths
            controller.Move(velocity * Time.deltaTime); //passing the velocito to the Move method
        }
            
    }