﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BaseGameController : MonoBehaviour
{
    //pause state
    bool paused;
    public GameObject explosionPrefab;

    //All the virtual funcitons are placeholder
    //Tdas las funciones que dicen virtual son fucniones
    //que sirven como interfaces para diferentes comportameinetos
    //la nuevas clases van a heredar de esta clase y podran utilizar
    //estas mismas funciones pero a su manera cada obj hijo
    public virtual void PlayerLostLife()
    {
        //deal with player life lost (update UI, etc)
    }

    //spawnear el palyer
    public virtual void SpawnPlayer()
    {
        //el palyer debe ser spawneado
    }

    public virtual void Respawn()
    {
        //el player reaparece
    }

    public virtual void StartGame()
    {
        //se hacen las acciones de arranque
        //funciones de arranque
    }

    
    //**
    //Explode es like a funtion library
    //en este caso hay una función explosión para ser
    //utilizada en cualquier parte

    //It's good to use a fx manager or explode manager
    //in order to separate the logic from the art
    //**

    public void Explode(Vector3 aPosition)
    {
        //instanciar una explosione en la aPosition
        //se utiliza la funcion Instantiate que crea una instancia
        //de un Prefab
        Instantiate(explosionPrefab, aPosition, Quaternion.identity);
    }

    public virtual void EnemyDestroyed(Vector3 aPosition, int pointValue, int hitByID)
    {
        //maneja al enemigo destruido
    }

    public virtual void BossDestroyed()
    {
        //maneja al boos cuando lo destruyen
    }

    public virtual void RestartGameButtonPressed()
    {
        //es el bo´ton de restart, generalemnte es cargar de nuevo la escena
        SceneManager.LoadScene("");
    }

    public bool Paused
    {
        //
        get
        {
            //get pause
            return paused;
        }
        set
        {
            //set paused
            paused = value;

            if(paused)
            {
                //pause time
                Time.timeScale = 0f;
            }
            else
            {
                //unpause
                Time.timeScale = 1f;
            }
        }
    }
}
