﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script is just for manage
//there is not for store data
//is just for declaring

public class BaseUserManager : MonoBehaviour
{
    //Los datos ne esta clase son privados para evitar algun 
    //tipo de corrupción de conflicto y por seguridad
    private int score;
    private int highScore;
    private int level;
    private int health;
    private bool isFinished;

    //nombre by default
    public string playerName = "Anon";

    public virtual void GetDefaultData()
    {
        playerName = "Anon";
        score = 0;
        level = 1;
        health = 3;
        highScore = 0;
        isFinished = false;
    }

    //API for player name
    public string GetName()
    {
        return playerName;
    }
    public void SetName(string aName)
    {
        playerName = aName;
    }

    //API for level
    public int GetLevel()
    {
        return level;
    }
    public void SetLevel(int num)
    {
        level = num;
    }

    //Score - HighScore
    public int GetHighScore()
    {
        return highScore;
    }
    public int GetScore()
    {
        return score;
    }

    //SCORE API
    public virtual void AddScore(int anAmount)
    {
        score += anAmount;
    }
    public void LostScore(int num)
    {
        score -= num;
    }
    public void SetScore(int num)
    {
        score = num;
    }

    //HEALTH API
    public int GetHealth()
    {
        return health;
    }
    public void AddHealth(int num)
    {
        health += num;
    }
    public void ReduceHealth(int num)
    {
        health -= num;
    }
    public void SetHealth(int num)
    {
        health = num;
    }

    public bool GetIsFinished()
    {
        return isFinished;
    }

    public void SetIsFinished(bool aVal)
    {
        isFinished = aVal;
    }
}
