﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OwnSceneManager : MonoBehaviour
{
    
    //this class needs some modification on the recient api
    public string[] levelNames;
    public int gameLevelNum;

    // Start is called before the first frame update
    void Start()
    {
        //mantener este objeto cargado en todas las escenas
        DontDestroyOnLoad(this.gameObject); 
    }

    public void LoadLevel(string sceneName)//by string
    {
        SceneManager.LoadScene(sceneName);
    }

    public void LoadLevel(int sceneName)//by number
    {
        SceneManager.LoadScene(sceneName);
    }

    public void GoNextLevel()
    {
        //si el indice de escenas sobrepasa el limite se resetea
        if (gameLevelNum >= levelNames.Length)
            gameLevelNum = 0;

        //carga del nivel
        LoadLevel(gameLevelNum);

        //incrementar el contador de niveles
        gameLevelNum++;
    }

    public void ResetGame()
    {
        //reatart los levels al contador 0
        gameLevelNum = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
