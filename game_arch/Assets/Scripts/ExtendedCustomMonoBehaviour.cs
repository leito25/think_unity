﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtendedCustomMonoBehaviour : MonoBehaviour
{
    //variables comunes para no repetir y seguramente tambien sirva para
    //optimizar recursos

    public Transform miTransform;
    public GameObject miGO;
    public Rigidbody miBody;
    public Rigidbody2D mi2DBody;

    public bool didInit;
    public bool canControl;

    public int id;

    [System.NonSerialized]
    public Vector3 tempVEC;

    [System.NonSerialized]
    public Transform tempTR;

    public virtual void SetID(int anID)
    {
        id = anID;
    }
}
