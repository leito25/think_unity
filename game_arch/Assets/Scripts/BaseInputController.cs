﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseInputController : MonoBehaviour
{
    //botones de dirección
    public bool Up;
    public bool Down;
    public bool Left;
    public bool Right;

    //fire
    public bool Fire1;

    //weapon - armas
    public bool Slot1;
    public bool Slot2;
    public bool Slot3;
    public bool Slot4;
    public bool Slot5;
    public bool Slot6;
    public bool Slot7;
    public bool Slot8;
    public bool Slot9;

    public float vert;
    public float horz;
    public bool shouldRespawn;

    public Vector3 TEMPVec3;
    private Vector3 zeroVector = new Vector3(0, 0, 0);

    public virtual void CheckInput()
    {
        //deal with input
        //en este caso solo la parte horizontal
        horz = Input.GetAxis("Horizontal");
        vert = Input.GetAxis("Vertical");

        //pero aui se puede montar todo lo que sea touch
        //o algun device extra

    }

    public virtual float GetHorizontal()
    {
        //retorna el valor horizontal
        return horz;
    }

    public virtual float GetVertical()
    {
        //retorna el valor horizontal
        return vert;
    }

    public virtual bool GetFire()
    {
        //retorna el valor horizontal
        return Fire1;
    }
    //LOS VIRTUAL SON PARA IMPLEMENTAR A LO
    //LARGO DEL JUEGO DESDE OTRAS PARTES
    //NO PRECISAMENTE SE DEFINE AQUI.. SOLO  se declara


    public bool GetRespawn()
    {
        //retorna el valor horizontal
        return shouldRespawn;
    }


    //the movement
    public virtual Vector3 GetMovementDirectionVector()
    {
        //el vector temporal
        TEMPVec3 = zeroVector;

        //para el valor a x
        if(Left || Right)
        {
            TEMPVec3.x = horz;
        }

        //para el valor de y
        if(Up || Down)
        {
            TEMPVec3.y = vert;
        }

        return TEMPVec3;
        //retorna los valores combinados en los componentes x y y
    }
}
