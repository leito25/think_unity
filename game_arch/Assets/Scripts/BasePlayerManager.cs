﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ste script se encarga de manejar los datos del player
//pero en una conexión entre el user manager, input manager y los controllers
//este si maneja estados del player a diferencia 
//del usermanager que solo declara las variables

public class BasePlayerManager : MonoBehaviour
{
    public bool didInit;

    //los scripts de managenment y de AI son publics, se
    //pueden instancias desde cualquier parte
    public BaseUserManager DataManager; //una instancia del user manager

    //aqui se utiliza awake porque awake se ejecuta antes de start
    public virtual void awake()
    {
        didInit = false;

        //se utiliza la funcon de init para hacer configuraciones previas a Start()
        Init();
    }

    public virtual void Init()
    {
        //Referencia a nuestro user manager
        DataManager = gameObject.GetComponent<BaseUserManager>();

        //si no existe osea si es null
        if (DataManager == null)
            DataManager = gameObject.AddComponent<BaseUserManager>();

        //aqui van las diferentes configuraciones y al final
        //va la expresion de cierre

        //

        //

        //
        didInit = true;//se deja la variable en true
    }

    //PROBABLEMENTE SERÁ LLAMADA DE AFUERA
    //DE LOS SCRIPTS EXTERIORES
    //se activa el juego la var finalizado esta en falso
    public virtual void GameStart()
    {
        DataManager.SetIsFinished(false);
    }


    //PROBABLEMENTE SERÁ LLAMADA DE AFUERA
    //DE LOS SCRIPTS EXTERIORES
    //juego terminado, se marca dentro de la instancia -true
    public virtual void GameFinished()
    {
        DataManager.SetIsFinished(true);
    }
}
